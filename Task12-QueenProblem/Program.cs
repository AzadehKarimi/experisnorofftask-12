﻿using System;

namespace Task12_QueenProblem
{
    class Program
    {

        static int[,] board;
        static int size = 8;


        static void Main(string[] args)
        {

            Console.WriteLine("tetetetete");

            board = new int[size, size];

            PrintBoard();

            Console.WriteLine("We need a coordinate set to place the first queen");
            var (x, y) = GetCoordinatesFromUser();


            if (TryInputCoordinates(x, y))
                Console.WriteLine("Solution Found");
            else
            {
                Console.WriteLine("No solution for this starting position");
            }
            PrintBoard();
        }


        static bool TryInputCoordinates(int x, int y)
        {

            board[y, x] = 1;

            int[] counters = new int[size];

            int currX = 0, currY = 0;

            bool allPossibilitiesExhausted = false;

            int startingColumn = (x == 0) ? 1 : 0;

            while (!allPossibilitiesExhausted)
            {

                // The current Y is equal to the columns counter value
                currY = counters[currX];

                // If this i a safe move, place the queen and move one column over. 
                if (IsSafeMove(currX, currY))
                {

                    // Go to next column after placing the queen
                    board[currY, currX] = 1;
                    currX++;

                    // If the new column is the same as the users inputted column, skip it.
                    if (currX == x) currX++;

                    // If the column number exceeds the board size, we have found a solution
                    if (currX >= size)
                    {
                        return true;
                    }
                }
                // If not a safe move, head to next row.
                else
                {

                    counters[currX]++;
                }


                // Backtracking 

                // If we reach the bottom of the column (counter reaches the size value)
                if (counters[currX] >= size)
                {

                    // If we reach the start after backtracking we've exhausted all posibilities
                    if (currX == startingColumn)
                    {
                        allPossibilitiesExhausted = true;
                    }

                    // Reset current columns counter
                    counters[currX] = 0;

                    // Move one column back
                    currX--;

                    // If the new column is the same as the users inputted column, skip it.
                    if (currX == x)
                    {
                        currX--;
                    }

                    // Remove the queen of the previous try, 
                    board[counters[currX], currX] = 0;

                    // Go to next row.
                    counters[currX]++;
                }
            }

            return false;
        }

        public static bool IsSafeMove(int x, int y)
        {

            int leftDiagonalX = x - Math.Max(x, y);
            int leftDiagonalY = y - Math.Max(x, y);

            int rightDiagonalX = x + y;
            int rightDiagonalY = y - y;

            for (int i = 0; i < size; i++)
            {

                // CHECK COLLUMN
                if (!IsValidCoordinate(x, y) || board[i, x] == 1 && i != y) return false;

                // CHECK ROW
                if (!IsValidCoordinate(x, y) || board[y, i] == 1 && i != x) return false;

                // CHECK LEFT TO RIGHT DIAGONAL \
                if (IsValidCoordinate(leftDiagonalX, leftDiagonalY))
                    if (board[leftDiagonalY, leftDiagonalX] == 1 && leftDiagonalX != x)
                        return false;

                // CHECK RIGHT TO LEFT DIAGONAL /
                if (IsValidCoordinate(rightDiagonalX, rightDiagonalY))
                    if (board[rightDiagonalY, rightDiagonalX] == 1 && rightDiagonalX != x)
                        return false;

                // Increment the diagonal counters
                leftDiagonalX++;
                leftDiagonalY++;

                rightDiagonalX--;
                rightDiagonalY++;
            }

            return true;
        }
        private static bool IsValidCoordinate(int x, int y)
        {
            return x >= 0 && x < size && y >= 0 && y < size;
        }

        private static void PrintBoard()
        {

            Console.WriteLine();

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    Console.Write($"{board[i, j]} ");
                }
                Console.WriteLine();
            }

            Console.WriteLine();
        }

        static (int, int) GetCoordinatesFromUser()
        {
            Console.WriteLine($"Please select a x (0-{size - 1}):");
            int x = int.Parse(Console.ReadLine());

            Console.WriteLine($"Now we need a y (0-{size - 1}):");
            int y = int.Parse(Console.ReadLine());

            return (x, y);
        }
    }
}
